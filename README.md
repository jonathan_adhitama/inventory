# README #

A simple RESTful API for a simple inventory system.

### How do I get set up? ###

* Install Python, Django, Django-Rest, and Pillow

### Running Server To access via WLAN / LAN ###
-python manage.py runserver 0.0.0.0:8000
-add Current machine's IP Address into settings.py under ALLOWED_HOSTS = [...]

### Administration Account ###
username: admin

password: test1234