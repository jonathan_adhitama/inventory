from __future__ import unicode_literals

from django.db import models

class Company(models.Model):
    def __str__(self):
        return self.company_name
    company_name = models.CharField(max_length=100, blank=False)

class Store(models.Model):
    def __str__(self):
        return self.store_name
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name="stores")
    store_name = models.CharField(max_length=100, blank=False)
    location = models.CharField(max_length=100, blank=False)
    class Meta:
        unique_together = ('store_name', 'location')
        ordering = ['id']
    def __unicode__(self):
        return '%s' % (self.store_name)

class Product(models.Model):
    def __str__(self):
        return self.product_name
    store = models.ForeignKey(Store, on_delete=models.CASCADE, related_name="products")
    product_name = models.CharField(max_length=100, blank=False)
    product_image = models.ImageField(upload_to='images/')
    stock = models.IntegerField(default=0, blank=False)
    def __unicode__(self):
        return "%s" % (self.product_name)

