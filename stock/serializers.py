from rest_framework import serializers
from stock.models import Company, Store, Product

class ProductSerializer(serializers.ModelSerializer):
    product_image = serializers.ImageField(max_length = None, use_url=True)
    # product_image = serializers.SerializerMethodField()
    # def get_product_image(self, instance):
    #     # returning image url if there is an image else blank string
    #     return instance.product_image.url if instance.product_image else ''

    class Meta:
        model = Product
        fields = ('id', 'product_name', 'product_image', 'stock', 'store')

class StoreSerializer(serializers.ModelSerializer):
    products = ProductSerializer(many=True, read_only=True)
    class Meta:
        model = Store
        fields = ('id', 'store_name', 'location', 'products', 'company')

class CompanySerializer(serializers.ModelSerializer):
    stores = StoreSerializer(many=True, read_only=True)
    class Meta:
        model = Company
        fields = ('id', 'company_name', 'stores')
