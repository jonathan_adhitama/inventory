from django.conf.urls import url
from stock import views
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    url(r'^company/$', views.CompanyList.as_view()),
    url(r'^company/(?P<pk>[0-9]+)/$', views.CompanyDetail.as_view()),
    url(r'^store/$', views.StoreList.as_view()),
    url(r'^store/(?P<pk>[0-9]+)/$', views.StoreDetail.as_view()),
    url(r'^product/$', views.ProductList.as_view()),
    url(r'^product/(?P<pk>[0-9]+)/$', views.ProductDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
